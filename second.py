#Disclaimer: I am not proficient in python
import asyncio
import logging

logger = logging.getLogger(__name__)
SLEEP_DURATION = os.getenv("SLEEP_DURATION")

class Pipeline:
    async def __init__(*args, **kwargs):
        default_sleep_duration = kwargs["default_sleep_duration"]
        async def sleep_for(coro, sleep_duration, *args, **kwargs):
        asyncio.sleep(sleep_duration)
        logger.info("Slept for %d seconds", sleep_duration) #use d instead of s because number
        start = datetime.now()
        await coro(*args, **kwarg)
        end = datetime.now()
        time_elapsed = (end - start).total_seconds() #switch start and end
        logger.debug(f"Executed the coroutine for {time_elapsed} seconds")

